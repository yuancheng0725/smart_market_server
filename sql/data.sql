set
  names utf8;
drop database if exists smart;
create database smart charset = utf8;
use smart;
create table huser(
    uuid INT PRIMARY KEY AUTO_INCREMENT,
    unickname VARCHAR(25),
    upwd VARCHAR(20),
    uemail VARCHAR(20)
  );
create table goods(
    gid INT PRIMARY KEY AUTO_INCREMENT,
    gtitle VARCHAR(50),
    gdesc VARCHAR(100),
    gimg VARCHAR(500),
    gserver VARCHAR(500),
    gcolor VARCHAR(50),
    gdetail VARCHAR(200),
    gxsimg VARCHAR(500),
    gsideid INT,
    gprice DECIMAL(7, 2),
    gnum INT
  );
CREATE TABLE ordered(
  orid varchar(50) PRIMARY KEY,
  userid int,
  useremail varchar(50),
  usernickname varchar(100),
  totalprice decimal(10,2),
  addr varchar(100),
  goodsList VARCHAR(100000000)
) ;
create table car(
    cid INT PRIMARY KEY AUTO_INCREMENT,
    userid INT,
    goodsList VARCHAR(100000000)
  );
create table gside(
    gsid INT PRIMARY KEY AUTO_INCREMENT,
    sname VARCHAR(10),
    simg VARCHAR(500)
  );
create table banner(
    bid INT PRIMARY KEY AUTO_INCREMENT,
    burl VARCHAR(500)
  );
INSERT INTO
  gside
VALUES(
    1,
    '苹果',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/1917047079/O1CN01xSVuU822AER6oscLb_!!2-item_pic.png_180x180.jpg_.webp'
  );
INSERT INTO
  gside
VALUES(
    2,
    '华为',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/2024314280/O1CN01t2f4Oe1hUHrHyVhUg_!!0-item_pic.jpg_180x180.jpg_.webp'
  );
INSERT INTO
  gside
VALUES(
    3,
    '小米',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2024314280/O1CN017cbe4f1hUHqovAc2i_!!0-item_pic.jpg_180x180.jpg_.webp'
  );
INSERT INTO
  gside
VALUES(
    4,
    '配件',
    'https://resource.smartisan.com/resource/8b0fe3117164dab7d91439b93dc112e0.png?x-oss-process=image/resize,w_216/format,webp'
  );
#用户表
INSERT INTO
  huser
VALUES(
    1001,
    'taosang',
    '123456',
    '123@qq.com'
  );
INSERT INTO
  huser
VALUES(
    1002,
    'liang',
    '6754321',
    '321@qq.com'
  );
#轮播图片表
INSERT INTO
  banner
VALUES(
    101,
    'https://www.apple.com.cn/v/home/ab/images/heroes/ipad-pro/hero_ipad_pro_non_avail__fcrsmhs4b7ma_largetall.jpg'
  );
INSERT INTO
  banner
VALUES(
    102,
    'https://www.apple.com.cn/v/home/ab/images/heroes/iphone-12-pro/iphone_12_pro_us__e5oyysg4k0ya_largetall.jpg'
  );
#产品表
INSERT INTO
  goods
VALUES(
    1,
    'nova 8',
    '麒麟芯片超级快充5G手机',
    'https://img.alicdn.com/imgextra/i1/2838892713/O1CN015hEGOu1VubAm7oQ5l_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/2838892713/O1CN01XdxpWQ1VubAqYXFSW_!!2838892713.jpg_430x430q90.jpg',
    '碎拼无忧原厂碎屏保一年',
    '亮黑色,森林绿',
    '8+128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2024314280/O1CN01hXiIHu1hUHrOxSPQ7_!!0-item_pic.jpg_180x180.jpg_.webp',
    2,
    3099.00,
    300
  );
INSERT INTO
  goods
VALUES(
    2,
    '华为畅想Z',
    '华为畅享Z5G全网通手机',
    'https://img.alicdn.com/imgextra/i4/2024314280/O1CN01xfapEU1hUHrOwO59r_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01ulkBFg1hUHrJPS3qx_!!2024314280.jpg_430x430q90.jpg',
    '赠送百元权益礼包',
    '深海蓝,樱雪晴空',
    '6+128GB',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i4/2024314280/O1CN01xfapEU1hUHrOwO59r_!!0-item_pic.jpg_180x180.jpg_.webp',
    2,
    2599.00,
    200
  );
INSERT INTO
  goods
VALUES(
    3,
    'nova 7 Pro',
    '曲面屏麒麟985芯片手机',
    'https://img.alicdn.com/imgextra/i2/2024314280/O1CN01Txky1E1hUHkvkHfhU_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01WH5ZPU1hUHl0ZCWlp_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/2024314280/O1CN012PId2Q1hUHl7DCKdd_!!2024314280.jpg_430x430q90.jpg',
    '下单选送豪礼、消费券',
    '7号色,森林绿,仲夏紫',
    '8+128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/2024314280/O1CN01xrruws1hUHnL687Dd_!!0-item_pic.jpg_180x180.jpg_.webp',
    2,
    4099.00,
    258
  );
INSERT INTO
  goods
VALUES(
    4,
    '荣耀50 Pro',
    '荣耀系列50Pro曲面屏手机',
    'https://img.alicdn.com/imgextra/i2/2024314280/O1CN01bjqwrd1hUHqqXw5hg_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN016wCEZk1hUHqdxlCjy_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01zVAzyn1hUHqartuy6_!!2024314280.jpg_430x430q90.jpg',
    '赠送百元礼包送5G卡',
    '夏日琥珀,初雪水晶,亮黑色',
    '8+256GB',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i2/2024314280/O1CN01bjqwrd1hUHqqXw5hg_!!0-item_pic.jpg_180x180.jpg_.webp',
    2,
    3700.00,
    74
  );
INSERT INTO
  goods
VALUES(
    5,
    'Mate 40',
    '华为Mate 40E 5G手机',
    'https://img.alicdn.com/imgextra/i4/2024314280/O1CN01E1bxvP1hUHrFKXOcg_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2024314280/O1CN01p2l5kb1hUHrDr7m84_!!2024314280.jpg_430x430q90.jpg',
    '赠送百元礼包送5G卡',
    '秘银色,亮黑色',
    '8+128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2024314280/O1CN01t2f4Oe1hUHrHyVhUg_!!0-item_pic.jpg_180x180.jpg_.webp',
    2,
    5888.00,
    125
  );
INSERT INTO
  goods
VALUES(
    6,
    'P40 Pro',
    '华为 P40 Pro 5G手机',
    'https://img.alicdn.com/imgextra/i4/2024314280/O1CN01kirGUv1hUHkgjmF48_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01XpP2RW1hUHkZDpIDt_!!2024314280.jpg_430x430q90.jpg',
    '官方正品全国联保',
    '冰霜银,深海蓝',
    '8+128GB',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i1/2024314280/O1CN014BVLe61hUHrNLuGkK_!!0-item_pic.jpg_180x180.jpg_.webp',
    2,
    7188.00,
    224
  );
INSERT INTO
  goods
VALUES(
    7,
    'nova8 Pro',
    'nova8 Pro 鸿蒙系统手机',
    'https://img.alicdn.com/imgextra/i3/2201227850912/O1CN01gp7j8b1IbjsDGJQYy_!!2-item_pic.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2201227850912/O1CN01yUxw1Y1IbjrHLl4Cw_!!2201227850912.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2201227850912/O1CN01l6QEL11IbjrKSRzAe_!!2201227850912.png_430x430q90.jpg',
    '赠送百元权益礼包',
    '8号色,普罗斯旺,亮黑色',
    '8+256GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i3/2201227850912/O1CN01gp7j8b1IbjsDGJQYy_!!2-item_pic.png_180x180.jpg_.webp',
    2,
    5929.00,
    212
  );
INSERT INTO
  goods
VALUES(
    8,
    '荣耀30 青春版',
    '荣耀30青春版5G手机',
    'https://img.alicdn.com/imgextra/i1/2024314280/O1CN01jJvlnQ1hUHlt5qS4m_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01wpHuZd1hUHlqabmi9_!!2024314280.jpg_430x430q90.jpg',
    '赠送百元权益礼包',
    '幻夜黑,绿野仙踪',
    '6+128GB',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/2024314280/O1CN01sHlFiL1hUHmeV9XVh_!!0-item_pic.jpg_180x180.jpg_.webp',
    2,
    1999.00,
    154
  );
INSERT INTO
  goods
VALUES(
    9,
    'iphone 12',
    'Apple/苹果 12 5G手机',
    'https://img.alicdn.com/imgextra/i1/2024314280/O1CN01ziMQGc1hUHqpguRJl_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01dA6cRE1hUHoSmHvii_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN0118yLKR1hUHrCR26ce_!!0-item_pic.jpg_430x430q90.jpg',
    '碎拼无忧原厂碎屏保一年',
    '白色,蓝色,紫色',
    '128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2024314280/O1CN0118yLKR1hUHrCR26ce_!!0-item_pic.jpg_230x230.jpg_.webp',
    1,
    6298.00,
    97
  );
INSERT INTO
  goods
VALUES(
    10,
    'iphone 12 Pro Max',
    'Apple 12 Pro Max 5G手机',
    'https://img.alicdn.com/imgextra/i4/2024314280/O1CN01peQiLJ1hUHqmEC1k5_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/6000000001500/O1CN01XNRdhC1Mx2k0Mnd6u_!!6000000001500-0-remus.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/6000000007458/O1CN01xwcpbk24xoV9KcUCm_!!6000000007458-0-remus.jpg_430x430q90.jpg',
    '碎拼无忧原厂碎屏保一年',
    '石墨色,金色,海蓝色',
    '256GB',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/2024314280/O1CN01peQiLJ1hUHqmEC1k5_!!0-item_pic.jpg_230x230.jpg_.webp',
    1,
    10228.00,
    89
  );
INSERT INTO
  goods
VALUES(
    11,
    'iphone 11',
    'Apple/苹果 iPhone 11',
    'https://img.alicdn.com/imgextra/i4/1917047079/O1CN01OKzWC122AER0OLKmP_!!1917047079.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/1917047079/O1CN01GpropK22AENY6QKHu_!!2-item_pic.png_430x430q90.jpg',
    '官方正品全国联保',
    '紫色,黄色,黑色',
    '128G',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i2/1917047079/O1CN01FEmrei22AER3xLkYS_!!2-item_pic.png_180x180.jpg_.webp',
    1,
    5299.00,
    32
  );
INSERT INTO
  goods
VALUES(
    12,
    'iphone 12 mini',
    'Apple/苹果iPhone 12 mini 5G手机',
    'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i4/1710936647/O1CN01Z3FySD1yyNLAq3Afv_!!1710936647.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i2/1710936647/O1CN01VnPuzJ1yyNKvNiZmr_!!1710936647.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i2/1710936647/O1CN01NrGmQL1yyNLSWGiYZ_!!1710936647.jpg_430x430q90.jpg',
    '碎拼无忧原厂碎屏保一年',
    '绿色,白色,蓝色',
    '128GB',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i2/1710936647/O1CN01ZHqIpe1yyNL5QidIf_!!0-item_pic.jpg_180x180.jpg_.webp',
    1,
    4938,
    112
  );
INSERT INTO
  goods
VALUES(
    13,
    'iphone XR',
    'Apple/苹果iphone XR手机',
    'https://gd2.alicdn.com/imgextra/i3/2632662225/O1CN01jL8Sdc1SJ5uvJrHOv_!!2632662225.jpg_400x400.jpg,https://gd2.alicdn.com/imgextra/i2/2632662225/O1CN01Gg1NJY1SJ5u7zdO5o_!!2632662225.jpg_400x400.jpg',
    '官方正品全国联保',
    '银色,白色',
    '128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i3/2632662225/O1CN01jL8Sdc1SJ5uvJrHOv_!!2632662225.jpg_180x180.jpg_.webp',
    1,
    2420.00,
    21
  );
INSERT INTO
  goods
VALUES(
    14,
    'ipad',
    'Apple/苹果ipad 10.2英寸平板',
    'https://img.alicdn.com/imgextra/i4/1917047079/O1CN01u2GAhO22AER8JKtYs_!!1917047079.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/1917047079/O1CN01ZYHvaJ22AENCvNKHS_!!2-item_pic.png_430x430q90.jpg',
    '官方正品全国联保',
    '深空灰色',
    '128GB',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/1917047079/O1CN017hGULu22AEQwsvbeI_!!2-item_pic.png_180x180.jpg_.webp',
    1,
    3299.00,
    56
  );
INSERT INTO
  goods
VALUES(
    15,
    '华为MatePad Pro',
    '12.6英寸麒麟9000E芯片平板',
    'https://img.alicdn.com/imgextra/i3/2207978890631/O1CN01WKXWzy1GX2bmYSsly_!!2207978890631.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2207978890631/O1CN01j8wId31GX2bgIL8Yz_!!2207978890631.png_430x430q90.jpg',
    '赠送百元权益礼包',
    '前白后冰霜银',
    '256GB',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i1/2207978890631/O1CN01q6s7C11GX2bkVrzdy_!!2-item_pic.png_180x180.jpg_.webp',
    2,
    5499.00,
    132
  );
INSERT INTO
  goods
VALUES(
    16,
    'Apple Watch SE',
    'Apple/苹果 Apple Watch SE',
    'https://img.alicdn.com/imgextra/i3/1917047079/O1CN01C4S4KB22AER1TrLgw_!!1917047079.jpeg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/1917047079/O1CN01p4nnJq22AERCCgQkn_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/1917047079/O1CN01BEYqpk22AENBSVXKS_!!0-item_pic.jpg_430x430q90.jpg',
    '赠送百元权益礼包',
    '银色铝',
    '40毫米',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/1917047079/O1CN01p4nnJq22AERCCgQkn_!!0-item_pic.jpg_180x180.jpg_.webp',
    1,
    2399.00,
    45
  );
INSERT INTO
  goods
VALUES(
    17,
    'Apple Watch S6',
    'Apple/苹果 Apple Watch Series 6',
    'https://img.alicdn.com/imgextra/i4/1917047079/O1CN01gBmN9g22AERCvFLm6_!!1917047079.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/1917047079/O1CN01hVLUcB22AERBPBF2f_!!2-item_pic.png_430x430q90.jpg',
    '官方正品全国联保',
    '金色铝',
    '40毫米',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/1917047079/O1CN01hVLUcB22AERBPBF2f_!!2-item_pic.png_180x180.jpg_.webp',
    1,
    3399.00,
    36
  );
INSERT INTO
  goods
VALUES(
    18,
    'iMac M1',
    '8 核中央处理器 7 核图形处理器',
    'https://img.alicdn.com/imgextra/i3/1917047079/O1CN01swPs5v22AEQIvqXlv_!!2-item_pic.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/1917047079/O1CN01PlCHX822AEQTDt3LY_!!2-item_pic.png_430x430q90.jpg',
    '官方正品全国联保',
    '蓝色',
    '官方标配',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/1917047079/O1CN01HlCZdl22AEQQDGlVb_!!2-item_pic.png_180x180.jpg_.webp',
    1,
    9599.00,
    13
  );
INSERT INTO
  goods
VALUES(
    19,
    'MacBook M1',
    '8 核中央处理器和 8 核图形处理器',
    'https://img.alicdn.com/imgextra/i1/1917047079/O1CN018Fxr0322AENyJS4dL_!!2-item_pic.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/1917047079/O1CN01LMileZ22AERM5Z76r_!!1917047079.png_430x430q90.jpg',
    '官方正品全国联保',
    '银白色',
    '官方标配',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i3/1917047079/O1CN01O0SjOZ22AERT9HnN8_!!0-item_pic.jpg_180x180.jpg_.webp',
    1,
    12745.00,
    21
  );
INSERT INTO
  goods
VALUES(
    20,
    'MateBook X Pro',
    '华为全面触摸屏便携办公本',
    'https://gd1.alicdn.com/imgextra/i1/3249920495/O1CN01w8EShq1FWkfoAeFsv_!!3249920495.jpg_400x400.jpg,https://gd4.alicdn.com/imgextra/i3/3249920495/O1CN01hqu21d1FWkfmwCQK7_!!3249920495.jpg_400x400.jpg',
    '赠送百元权益礼包',
    '星空灰',
    '官方标配',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/3249920495/O1CN01hqu21d1FWkfmwCQK7_!!3249920495.jpg_180x180.jpg_.webp',
    2,
    4550,
    31
  );
INSERT INTO
  goods
VALUES(
    21,
    'Mi 11 青春版',
    '小米11青春版5G手机',
    'https://img.alicdn.com/imgextra/i2/2024314280/O1CN01gRtL0p1hUHpqXxeYp_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01cskPxB1hUHprXJDie_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01KgHIrW1hUHpk9j4J5_!!2024314280.jpg_430x430q90.jpg',
    '官方正品全国联保',
    '冰峰黑提,清凉薄荷,清甜荔枝',
    '8+128GB',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/2024314280/O1CN01gRtL0p1hUHpqXxeYp_!!0-item_pic.jpg_180x180.jpg_.webp',
    3,
    2249.00,
    32
  );
INSERT INTO
  goods
VALUES(
    22,
    'MI MIX 4',
    '小米mix4智能网络手机',
    'https://img.alicdn.com/imgextra/i1/2205748077959/O1CN015JcEuG28fGzFlnGZQ_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2205748077959/O1CN01VKcaFB28fGz9jbTWm_!!2205748077959.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2205748077959/O1CN018qQMsn28fGzHOvMDy_!!2205748077959.jpg_430x430q90.jpg',
    '赠送百元权益礼包',
    '陶瓷黑',
    '12+512GB',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/2205748077959/O1CN015JcEuG28fGzFlnGZQ_!!0-item_pic.jpg_180x180.jpg_.webp',
    3,
    5499.00,
    56
  );
INSERT INTO
  goods
VALUES(
    23,
    'MI 11 Pro',
    '小米11 Pro5g骁龙888手机',
    'https://img.alicdn.com/imgextra/i4/2024314280/O1CN01VeWjnn1hUHpgsi6Ap_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2024314280/O1CN01Ra3y5p1hUHpeoZ8W8_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2024314280/O1CN01ctvyjl1hUHpfTanEd_!!2024314280.jpg_430x430q90.jpg',
    '意外碎屏保险',
    '黑色,紫色,绿色',
    '8+256GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/2024314280/O1CN01Ant1Ok1hUHqQrSDRh_!!0-item_pic.jpg_180x180.jpg_.webp',
    3,
    4799.00,
    92
  );
INSERT INTO
  goods
VALUES(
    24,
    'Redmi K40Pro+',
    '红米K40 Pro+ 5G手机',
    'https://img.alicdn.com/imgextra/i1/2024314280/O1CN017cbe4f1hUHqovAc2i_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/2024314280/O1CN01gaNCN21hUHqoulqDi_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/2024314280/O1CN01eWbtW71hUHpaKdjSR_!!2024314280.jpg_430x430q90.jpg',
    '意外碎屏保险',
    '幻境,晴雪,墨羽',
    '8+128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2024314280/O1CN017cbe4f1hUHqovAc2i_!!0-item_pic.jpg_180x180.jpg_.webp',
    3,
    2999.00,
    34
  );
INSERT INTO
  goods
VALUES(
    25,
    '华为nova7手机壳',
    '个性创意ins风彩色',
    'https://img.alicdn.com/imgextra/i4/2456268107/O1CN015L4n1N29l3YuiugLz_!!2456268107.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2456268107/O1CN015L4n1N29l3YuiugLz_!!2456268107.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/2456268107/O1CN01BypQ8N29l3Ypwqm1M_!!2456268107.jpg_430x430q90.jpg',
    '【收藏加购】送壁纸+挂环',
    '多彩白,多彩紫,多彩黑',
    '标配',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2456268107/O1CN018u62G029l3Yu7blWz_!!0-item_pic.jpg_180x180.jpg_.webp',
    4,
    19.80,
    210
  );
INSERT INTO
  goods
VALUES(
    26,
    'iPhone12pro max手机壳',
    '液态硅胶镜头全包防摔',
    'https://img.alicdn.com/imgextra/i1/2152686551/O1CN01DlyWOo1yGPIiLEty8_!!2152686551.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2152686551/O1CN01dyU9sW1yGPIbzwVto_!!2152686551.jpg_430x430q90.jpg',
    '送膜+挂绳',
    '烟灰蓝,草紫色',
    '液态硅胶',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2152686551/O1CN01bxvANd1yGPIeDSw9K_!!0-item_pic.jpg_180x180.jpg_.webp',
    4,
    20.50,
    73
  );
INSERT INTO
  goods
VALUES(
    27,
    '红米k40手机壳',
    '镜头全包保护redmi磨砂套',
    'https://img.alicdn.com/imgextra/i3/2258873958/O1CN01xBVTSd1f6oJYiyqyN_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/2258873958/O1CN01Jaxrhj1f6oJUtB3PN_!!2258873958.jpg_430x430q90.jpg',
    '7天无理由退换',
    '浅灰,深黑',
    '磨砂',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i3/2258873958/O1CN01xBVTSd1f6oJYiyqyN_!!0-item_pic.jpg_180x180.jpg_.webp',
    4,
    15.70,
    221
  );
INSERT INTO
  goods
VALUES(
    28,
    '手绘花朵手机壳',
    '网红文艺小清新苹果手机壳',
    'https://img.alicdn.com/imgextra/i2/2208799174460/O1CN01VxuZVV1ioj8qy1XDu_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/2208799174460/O1CN01Q9mfcJ1ioj8oQNQgg_!!2208799174460.jpg_430x430q90.jpg',
    '七天无理由退换',
    '透明花朵',
    '标配',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/2208799174460/O1CN01VxuZVV1ioj8qy1XDu_!!0-item_pic.jpg_180x180.jpg_.webp',
    4,
    9.80,
    51
  );
INSERT INTO
  goods
VALUES(
    29,
    '红米note10',
    'Redmi Note 10 5G',
    'https://img.alicdn.com/imgextra/i1/2205748077959/O1CN01yQqtHd28fGyBWPRzy_!!2205748077959.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/2205748077959/O1CN015m3OKO28fGyEBhnSI_!!2205748077959.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2205748077959/O1CN01BMZkbO28fGyHGFB50_!!2205748077959.jpg_430x430q90.jpg',
    '免费领取一张5G专属号卡',
    '星云灰,月影银',
    '6+128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/2205748077959/O1CN01ks490j28fGyCm71RS_!!0-item_pic.jpg_230x230.jpg_.webp',
    3,
    1179,
    46
  );
INSERT INTO
  goods
VALUES(
    30,
    '红米note9',
    'Redmi Note9天玑800U',
    'https://img.alicdn.com/imgextra/i2/725677994/O1CN01ujRlgq28vIpCxMgF1_!!725677994.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/2207895997260/O1CN01L0GzO923V87G3jsKb_!!2207895997260.jpg_430x430q90.jpg',
    '一年意外碎屏险',
    '云墨灰,青山外',
    '6+128GB',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i3/2207895997260/O1CN01ezBXuP23V88a82d4C_!!0-item_pic.jpg_230x230.jpg_.webp',
    3,
    1199,
    23
  );
INSERT INTO
  goods
VALUES(
    31,
    '红米note10 Pro',
    'Redmi Note 10Pro 5G手机',
    'https://img.alicdn.com/imgextra/i2/2024314280/O1CN014gjYUb1hUHqQVzjy7_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/2024314280/O1CN010zwWUj1hUHqjs7qMB_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/2024314280/O1CN01cmJqip1hUHqQW6aeY_!!0-item_pic.jpg_430x430q90.jpg',
    '赠送百元权益礼包',
    '月魄,幻青,星纱',
    '8+128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i3/2024314280/O1CN010zwWUj1hUHqjs7qMB_!!0-item_pic.jpg_230x230.jpg_.webp',
    3,
    1919,
    24
  );
INSERT INTO
  goods
VALUES(
    32,
    '小米11 5G',
    '小米11 5G手机骁龙888',
    'https://img.alicdn.com/imgextra/i4/2024314280/O1CN01bu7Mq71hUHqxmj0jv_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/2024314280/O1CN013mlobg1hUHqKqkWMy_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2024314280/O1CN01Q9Cpyr1hUHoLI4KUA_!!2024314280.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/2024314280/O1CN01WvQgqu1hUHoH0yf2T_!!2024314280.jpg_430x430q90.jpg',
    '增百元权益礼包',
    '黑色,蓝色,白色,烟紫',
    '8+256GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/2024314280/O1CN01bu7Mq71hUHqxmj0jv_!!0-item_pic.jpg_230x230.jpg_.webp',
    3,
    3999,
    23
  );
INSERT INTO
  goods
VALUES(
    33,
    '华为nova7Pro手机膜',
    '磨砂水凝膜',
    'https://gd2.alicdn.com/imgextra/i2/2549584202/O1CN01sfQ1yA1guYrhMXeBQ_!!2549584202.jpg_400x400.jpg,https://gd3.alicdn.com/imgextra/i3/2549584202/O1CN010aKifW1guYrj0AtTJ_!!2549584202.jpg_400x400.jpg',
    '运费险10元',
    '磨砂水凝,磨砂蓝光',
    '标配',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/2549584202/O1CN01sfQ1yA1guYrhMXeBQ_!!2549584202.jpg_230x230.jpg_.webp',
    4,
    18.9,
    22
  );
INSERT INTO
  goods
VALUES(
    34,
    '苹果USB数据线',
    'Apple/苹果 闪电转 USB 连接线 (1 米)',
    'https://img.alicdn.com/imgextra/i4/1917047079/O1CN01lm5QuK22AEK4riJZ1_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/1917047079/O1CN01aTYc3v22AEK5tEM42_!!1917047079.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/1917047079/O1CN01YGNenq22AEK5Qp5B0_!!1917047079.jpg_430x430q90.jpg',
    '运费险10元',
    '白色',
    '1米长',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i4/1917047079/O1CN01lm5QuK22AEK4riJZ1_!!0-item_pic.jpg_230x230.jpg_.webp',
    4,
    145,
    22
  );
INSERT INTO
  goods
VALUES(
    35,
    '苹果watch充电线',
    'Apple/苹果 Apple Watch 磁力充电线 (1 米)',
    'https://img.alicdn.com/imgextra/i2/1917047079/O1CN013ZCDT222AEJNtN1IU_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/1917047079/O1CN01A9yIbY22AEJPAhxka_!!1917047079.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/1917047079/O1CN01pmHy5L22AEJKMH5JW_!!1917047079.jpg_430x430q90.jpg',
    '运费险10元',
    '白色',
    '1米长',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/1917047079/O1CN013ZCDT222AEJNtN1IU_!!0-item_pic.jpg_230x230.jpg_.webp',
    4,
    283,
    33
  );
INSERT INTO
  goods
VALUES(
    36,
    '多端口转换器',
    'Satechi Aluminum USB-C Multiport Pro',
    'https://img.alicdn.com/imgextra/i2/1917047079/O1CN01RFlbro22AEISEgVXY_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i3/1917047079/O1CN01rANXjY22AEIRqkUxU_!!1917047079.jpeg_430x430q90.jpg',
    '运费险10元',
    '银灰',
    '标配',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/1917047079/O1CN01RFlbro22AEISEgVXY_!!0-item_pic.jpg_230x230.jpg_.webp',
    4,
    498,
    22
  );
INSERT INTO
  goods
VALUES(
    37,
    'USB Type-C',
    '华为5A数据线USB',
    'https://img.alicdn.com/imgextra/i3/2838892713/O1CN01EU5ch11Vub95jwvEI_!!2838892713-0-lubanu-s.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2838892713/O1CN01sjJxYW1Vub9HV8OZg_!!2838892713-0-lubanu-s.jpg_430x430q90.jpg',
    '运费险10元',
    '白色',
    '1米长',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/2838892713/O1CN01EU5ch11Vub95jwvEI_!!2838892713-0-lubanu-s.jpg_230x230.jpg_.webp',
    4,
    69,
    22
  );
INSERT INTO
  goods
VALUES(
    38,
    '快充连接线',
    'mophie USB-C 闪电接头快充连接线 (2 米)',
    'https://img.alicdn.com/imgextra/i2/1917047079/O1CN01YTSkLX22AEIRfwHs6_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/1917047079/O1CN01BdM9Vp22AEITUt5dQ_!!1917047079.jpeg_430x430q90.jpg',
    '运费险10元',
    '黑色',
    '2米长',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i2/1917047079/O1CN01YTSkLX22AEIRfwHs6_!!0-item_pic.jpg_230x230.jpg_.webp',
    4,
    278,
    22
  );
INSERT INTO
  goods
VALUES(
    39,
    'hdmi转vga转换器',
    '带音频视接口hdmi',
    'https://img.alicdn.com/imgextra/i4/725677994/O1CN01NAl56b28vIq3YsqCM_!!725677994.jpg_430x430q90.jpg',
    '运费险10元',
    '深邃黑',
    '标配',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i4/725677994/O1CN01czbOqx28vIpxIvZQb_!!0-item_pic.jpg_230x230.jpg_.webp',
    4,
    29.9,
    23
  );
INSERT INTO
  goods
VALUES(
    40,
    '小米充电器120W',
    '手机适配器原装官方正品',
    'https://img.alicdn.com/imgextra/i3/1714128138/O1CN01U6svNE29zFq9LxqYz_!!0-item_pic.jpg_430x430q90.jpg',
    '运费险10元',
    '纯净白',
    '120W',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/1714128138/O1CN01U6svNE29zFq9LxqYz_!!0-item_pic.jpg_230x230.jpg_.webp',
    4,
    249,
    33
  );
INSERT INTO
  goods
VALUES(
    41,
    '绿联30w充电器头',
    '适用于苹果11max手机ipadpro2020/2018平',
    'https://img.alicdn.com/imgextra/i3/725677994/O1CN01tkL2ir28vIpVZSOu0_!!0-item_pic.jpg_430x430q90.jpg',
    '运费险10元',
    '白色',
    '30W',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i3/725677994/O1CN01tkL2ir28vIpVZSOu0_!!0-item_pic.jpg_230x230.jpg_.webp',
    4,
    49,
    32
  );
INSERT INTO
  goods
VALUES(
    42,
    'Mi Pro15',
    'Xiaomi/小米笔记本 Pro15 酷睿i5超轻薄',
    'https://gd1.alicdn.com/imgextra/i2/2229264887/O1CN01ZnkHnT1lyIGRHD7iZ_!!2229264887.jpg_400x400.jpg,https://gd4.alicdn.com/imgextra/i4/2229264887/O1CN01cqo4O91lyIGPjVNRz_!!2229264887.jpg_400x400.jpg',
    '七天无理由退换',
    '银灰',
    '16G内存+512GB固态硬盘',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i2/2229264887/O1CN01ZnkHnT1lyIGRHD7iZ_!!2229264887.jpg_230x230.jpg_.webp',
    3,
    7199,
    34
  );
INSERT INTO
  goods
VALUES(
    43,
    'RedmiBook 13 Air',
    '2020款轻薄十代i7酷睿2K屏',
    'https://gd3.alicdn.com/imgextra/i3/0/O1CN01xFScOJ2NERzpkH8qR_!!0-item_pic.jpg_400x400.jpg,https://gd2.alicdn.com/imgextra/i2/1072539931/O1CN01zhh0cs2NES01dtaV0_!!1072539931.jpg_400x400.jpg',
    '七天无理由退换',
    '灰白',
    '16G内存+512GB固态硬盘',
    'https://g-search1.alicdn.com/img/bao/uploaded/i4/i3/1072539931/O1CN01xFScOJ2NERzpkH8qR_!!0-item_pic.jpg_230x230.jpg_.webp',
    3,
    4199,
    43
  );
INSERT INTO
  goods
VALUES(
    44,
    'Apple Watch Series 3',
    '空灰色铝金属表壳',
    'https://img.alicdn.com/imgextra/i1/1917047079/TB1AncBa3TqK1RjSZPhXXXfOFXa_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/1917047079/TB2iMQVa4naK1RjSZFBXXcW7VXa_!!1917047079.jpg_430x430q90.jpg',
    '七天无理由退换',
    '黑色',
    '运动型表带',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i1/1917047079/TB1AncBa3TqK1RjSZPhXXXfOFXa_!!0-item_pic.jpg_230x230.jpg_.webp',
    1,
    1499,
    34
  );
INSERT INTO
  goods
VALUES(
    45,
    'MacBook Air Apple M1',
    '配备 8 核中央处理器和 8 核图形处理器',
    'https://img.alicdn.com/imgextra/i1/1917047079/O1CN01L2vr6022AEO4QlLvx_!!2-item_pic.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/1917047079/O1CN01B5YVq122AENuxGjpN_!!2-item_pic.png_430x430q90.jpg',
    '七天无理由退换',
    '玫瑰金,白色',
    '8GB内存+512GB固态',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/1917047079/O1CN01B5YVq122AENuxGjpN_!!2-item_pic.png_230x230.jpg_.webp',
    1,
    9799,
    34
  );
INSERT INTO
  goods
VALUES(
    46,
    'Apple Watch Nike SE',
    '银色铝金属表壳',
    'https://img.alicdn.com/imgextra/i2/1917047079/O1CN01RoYdKV22AER6pWlSK_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/1917047079/O1CN01Mm2u7V22AERCtRuED_!!1917047079.jpeg_430x430q90.jpg',
    '赠送百元权益礼包',
    '银色',
    '铝金属表壳',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/1917047079/O1CN01RoYdKV22AER6pWlSK_!!0-item_pic.jpg_230x230.jpg_.webp',
    1,
    2499,
    52
  );
INSERT INTO
  goods
VALUES(
    47,
    ' iPad Pro',
    'Apple/苹果 12.9 英寸 iPad Pro',
    'https://img.alicdn.com/imgextra/i4/1917047079/O1CN01YWh8Pm22AEQTDqR4k_!!2-item_pic.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/1917047079/O1CN01qC9RZb22AEQNIo08f_!!2-item_pic.png_430x430q90.jpg',
    '七天无理由退换',
    '深空灰色,银色',
    '1TB存储',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/1917047079/O1CN01YWh8Pm22AEQTDqR4k_!!2-item_pic.png_230x230.jpg_.webp',
    1,
    14099,
    4
  );
INSERT INTO
  goods
VALUES(
    48,
    'iPhone SE',
    'Apple/苹果 iPhone SE',
    'https://img.alicdn.com/imgextra/i4/1917047079/O1CN012wMB4722AENTVJ8wh_!!2-item_pic.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i2/1917047079/O1CN01XhSdVa22AENVWCEBp_!!2-item_pic.png_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/1917047079/O1CN01Do8qAY22AENZuz9sj_!!2-item_pic.png_430x430q90.jpg',
    '七天无理由退换',
    '白色,黑色,红色',
    '128GB',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i4/1917047079/O1CN012wMB4722AENTVJ8wh_!!2-item_pic.png_230x230.jpg_.webp',
    1,
    3799,
    5
  );
INSERT INTO
  goods
VALUES(
    49,
    '腾讯黑鲨游戏手机3',
    ' 骁龙865 270Hz触控采样率',
    'https://img.alicdn.com/imgextra/i3/2205748077959/O1CN01fpYefr28fGt7FEdPh_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i4/2205748077959/O1CN01kjRk8Z28fGsEUvpmK_!!2205748077959.jpg_430x430q90.jpg',
    '意外碎屏保险',
    '黑色,白色',
    '128GB',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i3/2205748077959/O1CN01fpYefr28fGt7FEdPh_!!0-item_pic.jpg_230x230.jpg_.webp',
    3,
    3299,
    5
  );
INSERT INTO
  goods
VALUES(
    50,
    '荣耀30',
    '荣耀30手机50倍远摄 麒麟985 5G手机',
    'https://img.alicdn.com/imgextra/i2/2205748077959/O1CN012amPZx28fGyfLZJpG_!!2205748077959.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2205748077959/O1CN01GXRuL328fGyeTVMM5_!!0-item_pic.jpg_430x430q90.jpg,https://img.alicdn.com/imgextra/i1/2205748077959/O1CN01GSjTcj28fGygtRCQn_!!2205748077959.jpg_430x430q90.jpg',
    '意外碎屏保险',
    '钛空银,幻夜黑,流光幻境',
    '8+128GB',
    'https://g-search2.alicdn.com/img/bao/uploaded/i4/i1/2205748077959/O1CN01GXRuL328fGyeTVMM5_!!0-item_pic.jpg_230x230.jpg_.webp',
    2,
    3699,
    12
  );
INSERT INTO
  goods
VALUES(
    51,
    'MagicBooK 15',
    '华为荣耀 2020新款商务办公本',
    'https://gd1.alicdn.com/imgextra/i2/275072910/O1CN01hEtnJn1XMpAHmzTF3_!!275072910.jpg_400x400.jpg,https://gd4.alicdn.com/imgextra/i4/275072910/O1CN01EFVhe61XMpALox4IT_!!275072910.jpg_400x400.jpg,https://gd2.alicdn.com/imgextra/i2/275072910/O1CN01DhQ7BT1XMpALox8Sc_!!275072910.jpg_400x400.jpg',
    '七天无理由退换',
    '冰川银',
    '16GB内存+512GB固态',
    'https://g-search3.alicdn.com/img/bao/uploaded/i4/i2/275072910/O1CN01hEtnJn1XMpAHmzTF3_!!275072910.jpg_230x230.jpg_.webp',
    2,
    3620,
    235
  );