const query = require('./db')
// 重新获得更新后数据工具函数
module.exports.getCarGoods = async function getCarGoods(userid){
    let sql = `select * from car where userid = ${userid}`
    let result = await query(sql)
    if(result.length === 1){
        return JSON.parse(result[0].goodsList)
    }else{
        return []
    }
}