const mysql = require('mysql')
const config = {
    user: 'root',
    database: 'smart',
    host: '127.0.0.1',
    password: '',
    prot: '3306'
}
const pool = mysql.createPool(config)

function query(sqlStr,params){
    return new Promise((resolve,reject) => {
        pool.query(sqlStr,params,(err,res) => {
            if(err){
                reject(err)
            }else{
                resolve(res)
            }
        })
    })
}

module.exports = query