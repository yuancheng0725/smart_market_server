const Router = require('koa-router')
const search = new Router()
const query = require('../util/db')

//根据搜索内容查找商品
//http://127.0.0.1:8080/search/good?name=nova
search.get('/good', async ctx => {
    let name = ctx.query.name
    let sql1 = `select * from goods`;
    let result1 = await query(sql1)
    if(result1.length !== 0){
        let group = []
        for(let item of result1){
            let flag = item.gtitle.indexOf(name)
            if(flag !== -1){
                group.push({
                    gid: item.gid,
                    gtitle: item.gtitle
                })
            }
        }
        ctx.body = {
            code: 200,
            msg: '搜索成功',
            data: group
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '检索商品失败',
            data: []
        }
    }
})

module.exports = search