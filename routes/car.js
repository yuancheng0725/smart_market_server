const Router = require('koa-router')
const query = require('../util/db')
const car = new Router()
const getCarGoods = require('../util/car_getCarGoods').getCarGoods

// 用户添加商品至购物车
// http://127.0.0.1:8080//car/addGoods
car.put('/addGoods', async ctx => {
    const {
        userid,
        gid,
        gname,
        gimg,
        gcount,
        gprice,
        gcolor
    } = ctx.request.body
    let obj = {
        gid,
        gname,
        gimg,
        gcount,
        gprice,
        gcolor
    }
    // 如果该用户未拥有购物车列表则直接创建
    let sql1 = `select * from car where userid = ${userid}`
    let result1 = await query(sql1)
    if (result1.length === 0) {
        let list = JSON.stringify([obj])
        let sql2 = `INSERT INTO car (userid,goodsList)VALUES(?,?)`
        let result2 = await query(sql2, [userid, list])
        if (result2.affectedRows === 0) {
            ctx.body = {
                code: 201,
                msg: '添加购物车失败',
                data: await getCarGoods(userid)
            }
        } else {
            ctx.body = {
                code: 200,
                msg: '添加购物车成功',
                data: await getCarGoods(userid)
            }
        }
    } else {
        // 将该用户的goodsList转换成JSON格式
        let goodsList = JSON.parse(result1[0].goodsList)
        const log = goodsList.findIndex(item => item.gid == gid && item.gcolor == gcolor)
        if (log === -1) {
            // 如果未查找到一模一样的商品
            goodsList = [...goodsList, obj]
        } else {
            // 如果查找到一模一样的商品
            // 增加商品数量
            goodsList[log].gcount = goodsList[log].gcount * 1 + 1
        }
        goodsList = JSON.stringify(goodsList)
        let sql3 = `update car set goodsList = ? where userid = ?`
        let result3 = await query(sql3, [goodsList, userid])
        if (result3.affectedRows === 1) {
            ctx.body = {
                code: 200,
                msg: '添加购物车成功',
                data: await getCarGoods(userid)
            }
        } else {
            ctx.body = {
                code: 201,
                msg: '添加购物车失败',
                data: await getCarGoods(userid)
            }
        }

    }
})

// 根据用户id查询购物车信息
// http://127.0.0.1:8080/car/getGoods?userid=1
car.get('/getGoods', async ctx => {
    let userid = ctx.request.query.userid
    let sql = `select * from car where userid = ?`
    let result = await query(sql, [userid])
    if (result.length === 0) {
        ctx.body = {
            code: 201,
            msg: '该用户还未添加任何商品',
            data: []
        }
    } else {
        let data = JSON.parse(result[0].goodsList);
        if (data.length === 0) {
            ctx.body = {
                code: 201,
                msg: '该用户还未添加任何商品',
                data
            }
        } else {
            ctx.body = {
                code: 200,
                msg: '查询购物车信息成功',
                data
            }
        }
    }
})

// 用户直接删除购物车内选中的整类商品
// http://127.0.0.1:8080/car/delGoods
car.put('/delGoods', async ctx => {
    const {
        userid,
        gid,
        gcolor
    } = ctx.request.body
    let sql1 = `select * from car where userid = ${userid}`
    let result1 = await query(sql1)
    if (result1.length === 0) {
        ctx.body = {
            code: 201,
            msg: '该用户还未添加任何商品,无法删除该商品',
            data: await getCarGoods(userid)
        }
    } else {
        let goodsList = result1[0].goodsList
        goodsList = JSON.parse(goodsList)
        let log = goodsList.findIndex(item => item.gid == gid && item.gcolor == gcolor)
        if (log == -1) {
            ctx.body = {
                code: 202,
                msg: '无法删除该用户未添加的商品',
                data: await getCarGoods(userid)
            }
        } else {
            goodsList.splice(log, 1)
            goodsList = JSON.stringify(goodsList)
            let sql3 = `update car set goodsList = ? where userid = ?`
            let result3 = await query(sql3, [goodsList, userid])
            if (result3.affectedRows === 1) {
                ctx.body = {
                    code: 200,
                    msg: '删除商品成功',
                    data: await getCarGoods(userid)
                }
            } else {
                ctx.body = {
                    code: 203,
                    msg: '删除商品失败',
                    data: await getCarGoods(userid)
                }
            }
        }
    }
})

// 清空用户购物车
// http://127.0.0.1:8080/car/clearGoods?userid=1004
car.get('/clearGoods', async ctx => {
    let userid = ctx.request.query.userid
    let goodsList = JSON.stringify([])
    let sql = `update car set goodsList = ? where userid = ?`
    let result = await query(sql, [goodsList, userid])
    if (result.affectedRows === 1) {
        ctx.body = {
            code: 200,
            msg: '清空购物车成功',
            data: await getCarGoods(userid)
        }
    } else {
        ctx.body = {
            code: 201,
            msg: '清空购物车失败',
            data: await getCarGoods(userid)
        }
    }
})

// 更改商品数量
// http://127.0.0.1:8080/car/changeCount
car.put('/changeCount', async ctx => {
    let {
        userid,
        gid,
        gcolor,
        type
    } = ctx.request.body
    let sql1 = `select * from car where userid = ?`
    let result1 = await query(sql1, [userid])
    let goodsList = JSON.parse(result1[0].goodsList)
    const log = goodsList.findIndex(item => {
        return item.gid == gid && item.gcolor == gcolor
    })
    if (type == 'add') {
        goodsList[log].gcount = goodsList[log].gcount * 1 + 1
    } else {
        if (goodsList[log].gcount == 1) return
        goodsList[log].gcount = goodsList[log].gcount * 1 - 1
    }
    goodsList = JSON.stringify(goodsList)
    let sql2 = `update car set goodsList = ? where userid = ?`
    let result2 = await query(sql2, [goodsList, userid])
    if (result2.affectedRows === 1) {
        ctx.body = {
            code: 200,
            msg: '修改数量成功',
            data: await getCarGoods(userid)
        }
    } else {
        ctx.body = {
            code: 201,
            msg: '修改失败',
            data: await getCarGoods(userid)
        }
    }
})

module.exports = car