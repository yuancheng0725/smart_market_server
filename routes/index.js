const Router = require('koa-router')
const router = new Router()
// 引入路由
const banner = require('./banner')
const goods = require('./goods')
const side = require('./side')
const user = require('./user')
const car = require('./car')
const ordered = require('./ordered')
const search = require('./search')

// 启用路由接口
// 轮播图接口
router.use('/banner',banner.routes(),banner.allowedMethods())
// 产品接口
router.use('/goods',goods.routes(),goods.allowedMethods())
// 分类信息接口
router.use('/side',side.routes(),side.allowedMethods())
// 用户相关接口
router.use('/user',user.routes(),user.allowedMethods())
// 购物车相关接口
router.use('/car',car.routes(),car.allowedMethods())
// 订单相关接口
router.use('/order',ordered.routes(),ordered.allowedMethods())
// 搜索接口
router.use('/search',search.routes(),search.allowedMethods())

module.exports = router