const query = require('../util/db')
const Router = require('koa-router')
const banner = new Router()

// 获取轮播图图片
//http://127.0.0.1:8080/banner/get
banner.get('/get',async ctx => {
    let sql = `select * from banner`
    let result = await query(sql)
    if(result.length === 0){
        ctx.body = {
            code: 201,
            msg: '获取失败'
        }
        return
    }
    ctx.body = {
        code: 200,
        msg: '获取成功',
        data: result
    }
})


module.exports = banner