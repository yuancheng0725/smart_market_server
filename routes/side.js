const Router = require('koa-router')
const side = new Router()
const query = require('../util/db')

// 获取所有分类信息
//http://127.0.0.1:8080/side/all
side.get('/all',async ctx => {
    let sql = `select * from gside`
    let result = await query(sql)
    if(result.length === 0){
        ctx.body = {
            code: 201,
            msg: '未查询到相关分类信息'
        }
        return
    }
    ctx.body = {
        code: 200,
        msg: '查询分类信息成功',
        data: result
    }
})

module.exports = side